@echo off
title: "Dijitaller DNS Bakım"
cd %~dp0
color 0B
goto check_Permissions

:check_Permissions
    echo Yonetici yetkileri deneniyor

    net session >nul 2>&1
    if %errorLevel% == 0 (
        goto kurulum_baslangic
    ) else (
				    color 4F
        echo Lutfen bu dosyayi yonetici olarak acin
								echo %cd%
								pause
								exit
    )

:kurulum_baslangic 
ipconfig /flushdns

taskkill /F /IM iexplore.exe
taskkill /F /IM chrome.exe
taskkill /F /IM opera.exe
taskkill /F /IM firefox.exe

@echo off

:: Google DNS
set DNS1=1.1.1.1
set DNS2=8.8.4.4

for /f "tokens=1,2,3*" %%i in ('netsh int show interface') do (
    if %%i equ Enabled (
        echo Changing "%%l" : %DNS1% + %DNS2%
        netsh int ipv4 set dns name="%%l" static %DNS1% primary validate=no
        netsh int ipv4 add dns name="%%l" %DNS2% index=2 validate=no
    )
)

ipconfig /flushdns

:EOF
