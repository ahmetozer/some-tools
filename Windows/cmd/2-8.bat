@echo off
color F4
cls
title: "Dijitaller Uygulama Baslatici"


IF "%1"=="" (@echo off) ELSE set arg1=%1
IF "%2"=="" (@echo off) ELSE set arg2=%2
IF "%1"=="" (@echo off) ELSE goto %arg1%
MODE CON: COLS=100 LINES=30
goto check_Permissions

:check_Permissions
    echo Yonetici yetkileri deneniyor

    net session >nul 2>&1
    if %errorLevel% == 0 (
        goto dosyakontrol
    ) else (
				    color 4F
        echo Lutfen bu dosyayi yonetici olarak acin
								echo %cd%
								pause >nul
								exit
    )
:dosyakontrol
if exist %programdata%\Dijitaller\2-8\kapatilacaklar.txt (
    echo dosya var
				goto kurulum_baslangic
) else (
echo dosya yok
cls
goto krilim
)


:kurulum_baslangic 
color F4
cls

echo Seciminizi yapin
echo (1) Saat 2 de baslatilacak programlarin kisayollari
echo (2) Saat 2 de programlar baslatilsinmi
echo (3) Saat 8 de bilgisayar kapatilsin mi 
echo (4) Saat 8 de kapatilacak programlarin isimleri
echo (5) Saat 8 de programlar kapatilsin mi
echo (kaldir) 2-8 programini bilgisayarinizdan kaldirin
echo (e) Cikis
set /p secim="Secim: "

IF %secim%==1 (
goto saat2kisayol
)

IF %secim%==2 (
goto saat2start
)

IF %secim%==3 (
goto saat8pckapat
)

IF %secim%==4 (
goto saat8kapatilicaklar
)

IF %secim%==5 (
goto saat8prog
)

IF %secim%==e (
exit
)

IF %secim%==kaldir (
goto kurulum_kaldirici
)

echo lutfen listedekilere gore secim yapin
pause >nul
goto kurulum_baslangic


:saat2kisayol
start %programdata%\Dijitaller\2-8\startup
goto kurulum_baslangic


:saat8kapatilicaklar
start %programdata%\Dijitaller\2-8\kapatilacaklar.txt
goto kurulum_baslangic


:saat2start
cls
echo %programdata%\Dijitaller\2-8\startup icinde bulunan 
echo program kisayollarinin baslangicta calismasini istiyormusunuz ?
echo (y) Evet acilsin - (n) Hayir acilmasin
set /p secimsaat2="Secim: "

IF %secimsaat2%==y (
::SchTasks /Create /SC DAILY /TN Dijitaller-2-8-open /TR "%programdata%\Dijitaller\2-8\2-8.exe baslatici" /ST 02:00 2>NUL >nul
goto xmlmaker
)

IF %secimsaat2%==n (
SchTasks /Delete /TN Dijitaller-2-8-open /F 2>nul >nul
echo saat 2 baslangic programlari pasif
pause >nul
goto kurulum_baslangic
)

echo lutfen y ve n ile belirtiniz
pause >nul
goto saat2start


:saat8pckapat
cls
echo Bilgisayariniz sabah saat 8 de kapatilsin mi ?
echo (y) Evet kapatilsin - (n) Hayir kapatilmasin
set /p secimsaat8="Secim: "

IF %secimsaat8%==y (

SchTasks /Create /SC DAILY /TN Dijitaller-2-8-winoff /TR "%programdata%\Dijitaller\2-8\2-8.exe fastshutdown" /ST 08:01 /F 2>NUL >nul
echo Bilgisayariniz sabah 8 de kapatilmaya programlandi
pause >nul
goto kurulum_baslangic 

)

IF %secimsaat8%==n (

SchTasks /Delete /TN Dijitaller-2-8-winoff /F 2>NUL >nul
echo Bilgisayariniz sabah 8 de kaptilmayacak
pause >nul
goto kurulum_baslangic 

)

echo lutfen y ve n ile belirtiniz
pause >nul
goto saat8pckapat

:dasdfasdfasdgasgasdgsa

:saat8prog
cls
echo saat 8 de listede bulunan programlar kapatilsin mi
echo (y) Evet - (n) Hayir
set /p secimsaat8program="Secim: "

IF %secimsaat8program%==y (
SchTasks /Create /SC DAILY /TN Dijitaller-2-8-close /TR "%programdata%\Dijitaller\2-8\2-8.exe kapatici" /ST 08:00 /F 2>nul >nul
echo Listedeki programlar saat 8 de kapatilacak
pause >nul
goto kurulum_baslangic 
)

IF %secimsaat8program%==n (
SchTasks /Delete /TN Dijitaller-2-8-close /F 2>nul >nul
echo Listedeki programlar saat 8 de kapatilacaktir
pause >nul
goto kurulum_baslangic
)

echo lutfen y ve n ile belirtiniz
pause >nul
goto saat8prog






:baslatici

color FC
title: "Dijitaller Uygulama Baslatici"
MODE CON: COLS=70 LINES=10
echo Dijitaller Uygulama Baslatici
echo Uygulamalar birazdan baslatilacak
echo Baslatma islemini iptal etmak icin
echo CTRL+C tusuna yada X tiklayin
echo https://www.dijitaller.com
echo %time%
timeout 5 >nul


for %%v in ("%programdata%\Dijitaller\2-8\startup\*.lnk") do start "" "%%~v"
for %%v in ("%programdata%\Dijitaller\2-8\startup\*.url") do start "" "%%~v"
for %%v in ("%programdata%\Dijitaller\2-8\startup\*.torrent") do start "" "%%~v"
echo tum uygulamalar baslatildi
ping 127.0.0.1 -n 2 > NUL
exit












:fastshutdown
color FC
title: "Dijitaller Sistem Kapatici"
MODE CON: COLS=70 LINES=10
echo Dijitaller 2-8 Sistem Kapatici
echo Sisteminiz birazdan kapatilacak
echo Kapatma islemini iptal etmek icin
echo CTRL+C tusuna yada X tiklayin
echo https://www.dijitaller.com
echo %time%
timeout 5 >nul

start %windir%\System32\shutdown.exe /s /t 0
exit










:kapatici
cd %programdata%\Dijitaller\2-8
color FC
title: "Dijitaller Sistem Kapatici"
MODE CON: COLS=70 LINES=10
echo Dijitaller Sistem Kapatici
echo Sistem uygulamalari ve sistem 10 saniye icinde kapatilacaktir
echo Kapatma islemini iptal etmak icin
echo CTRL+C tusuna yada X tiklayin
echo https://www.dijitaller.com
ping -n 2 127.0.0.1 > NUL
color F0
ping -n 2 127.0.0.1 > NUL
color E0
ping -n 2 127.0.0.1 > NUL
color D0
ping -n 2 127.0.0.1 > NUL
color C0
ping -n 2 127.0.0.1 > NUL
color B0
ping -n 2 127.0.0.1 > NUL
color A0
ping -n 2 127.0.0.1 > NUL
color A0


@echo off
for /f "tokens=*" %%a in (kapatilacaklar.txt) do (
  taskkill /F /IM %%a >nul
)
exit

:krilim
color 79
echo Dijitaller.com 2-8 Kurulum
echo Bu programi ahmetozer.org ve dijitaller.com 
echo disindaki bir konumdan indirmeyiniz.
echo Farkli bir yerden indirdiyseniz guvenliginiz icin Dijitaller.com
echo adresini ziyaret ederek en son verisiyonu indirin.

echo Programi yuklemek istiyormusunuz2
set /p secimsaat8program="(y) Evet - (n) Hayir =  "

IF %secimsaat8program%==y (
goto krilim2
)
exit

:krilim2

echo %cd%

SchTasks /Delete /TN Dijitaller-2-8-open /F 2>NUL
SchTasks /Delete /TN Dijitaller-2-8-close /F 2>NUL
SchTasks /Delete /TN Dijitaller-2-8-winoff /F 2>NUL
mkdir %programdata%\Dijitaller\2-8\startup
copy 2-8.exe %programdata%\Dijitaller\2-8\ >NUL

set SCRIPT="%TEMP%\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs"

echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%USERPROFILE%\Desktop\2-8.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "%programdata%\Dijitaller\2-8\2-8.exe" >> %SCRIPT%
echo oLink.IconLocation = "%programdata%\Dijitaller\2-8\2-8.exe, 0" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%

cscript %SCRIPT% >nul
del %SCRIPT%


echo F | xcopy /q /y "%USERPROFILE%\Desktop\2-8.lnk" "%ProgramData%\Microsoft\Windows\Start Menu\Programs\2-8.lnk" >nul



echo Steam.exe > %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo SteamService.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo Steamwebhelper.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo upc.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo UplayWebCore.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo origin.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo OriginWebHelperService.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt
echo OriginThinSetupInternal.exe >> %programdata%\Dijitaller\2-8\kapatilacaklar.txt

color 72
echo kurulum tamamlandi
start https://www.dijitaller.com/gece-2-ile-8-arasinda-indirme-yapmak
timeout 3 >nul

goto kurulum_baslangic				


:xmlmaker
call :heredoc xml > 2-8startup.56845.xml && goto next2
<?xml version="1.0" encoding="UTF-16"?>
<Task version="1.2" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">
  <RegistrationInfo>
  </RegistrationInfo>
  <Principals>
    <Principal id="Author">
    </Principal>
  </Principals>
  <Settings>
    <DisallowStartIfOnBatteries>true</DisallowStartIfOnBatteries>
    <StopIfGoingOnBatteries>true</StopIfGoingOnBatteries>
    <MultipleInstancesPolicy>IgnoreNew</MultipleInstancesPolicy>
    <WakeToRun>true</WakeToRun>
    <IdleSettings>
      <StopOnIdleEnd>true</StopOnIdleEnd>
      <RestartOnIdle>false</RestartOnIdle>
    </IdleSettings>
  </Settings>
  <Triggers>
    <CalendarTrigger>
      <StartBoundary>2017-04-29T02:00:00</StartBoundary>
      <ScheduleByDay>
        <DaysInterval>1</DaysInterval>
      </ScheduleByDay>
    </CalendarTrigger>
  </Triggers>
  <Actions Context="Author">
    <Exec>
      <Command>C:\ProgramData\Dijitaller\2-8\2-8.exe</Command>
						<Arguments>baslatici</Arguments>
    </Exec>
  </Actions>
</Task>
:next2
SchTasks /Create /XML 2-8startup.56845.xml /TN Dijitaller-2-8-open /F 2>nul >nul
del 2-8startup.56845.xml
echo saat 2 baslangic programlari aktif
pause >nul
goto kurulum_baslangic



:kurulum_kaldirici
echo https://ahmetozer.org 
echo program 5 saniye icinde kaldirilacak
echo kaldirma islemini iptal etmek icin penceriyi kapatin

timeout 5 >nul

SchTasks /Delete /TN Dijitaller-2-8-open /F 2>nul
SchTasks /Delete /TN Dijitaller-2-8-close /F 2>nul
SchTasks /Delete /TN Dijitaller-2-8-winoff /F 2>nul
rmdir /S /Q %programdata%\Dijitaller\2-8
del "%ProgramData%\Microsoft\Windows\Start Menu\Programs\2-8.lnk" 2>nul
del "%USERPROFILE%\Desktop\2-8.lnk" 2>nul
color 2F

echo program kaldirildi 
start "2-8 uninstaller" cmd /c "ping -n 1 localhost & rmdir /S /Q %programdata%\Dijitaller\2-8"



goto :EOF












:heredoc <uniqueIDX>
setlocal enabledelayedexpansion
set go=
for /f "delims=" %%A in ('findstr /n "^" "%~f0"') do (
    set "line=%%A" && set "line=!line:*:=!"
    if defined go (if #!line:~1!==#!go::=! (goto :EOF) else echo(!line!)
    if "!line:~0,13!"=="call :heredoc" (
        for /f "tokens=3 delims=>^ " %%i in ("!line!") do (
            if #%%i==#%1 (
                for /f "tokens=2 delims=&" %%I in ("!line!") do (
                    for /f "tokens=2" %%x in ("%%I") do set "go=%%x"
                )
            )
        )
    )
)
goto :EOF
