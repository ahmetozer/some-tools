function youtubeuser() {
createSpotifyEmbed = (key) => {
  return '<div class="g-ytsubscribe" data-channel="' + key + '" data-layout="full" data-count="default"></div>';
};
transformSpotifyLinks = (text) => {
  const self = this;
  const fullreg = /(https?:\/\/)?(www\.)?(youtube\.com\/user\/)([^& \n<]+)(?:[^ \n<]+)?/g;
  const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/user\/)([^& \n<]+)(?:[^ \n<]+)?/g;
  const match = text.match(fullreg);
  if (match && match.length > 0) {
    let resultHtml = text;
    for (var i=0; i < match.length; i++) {
      let matchParts = match[i].split(regex);
      resultHtml = resultHtml.replace(match[i], self.createSpotifyEmbed(matchParts[1]));
    }
    return resultHtml;
  } else {
    return text;
  }
};
const htmlContent = document.getElementById('contentToTransform');
htmlContent.innerHTML = transformSpotifyLinks(htmlContent.innerHTML);
}

function youtubechannel() {
createSpotifyEmbed = (key) => {
  return '<div class="g-ytsubscribe" data-channelid="' + key + '" data-layout="full" data-count="default"></div>';
};
transformSpotifyLinks = (text) => {
  const self = this;
  const fullreg = /(https?:\/\/)?(www\.)?(youtube\.com\/channel\/)([^& \n<]+)(?:[^ \n<]+)?/g;
  const regex = /(?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/channel\/)([^& \n<]+)(?:[^ \n<]+)?/g;
  const match = text.match(fullreg);
  if (match && match.length > 0) {
    let resultHtml = text;
    for (var i=0; i < match.length; i++) {
      let matchParts = match[i].split(regex);
      resultHtml = resultHtml.replace(match[i], self.createSpotifyEmbed(matchParts[1]));
    }
    return resultHtml;
  } else {
    return text;
  }
};
const htmlContent = document.getElementById('contentToTransform');
htmlContent.innerHTML = transformSpotifyLinks(htmlContent.innerHTML);
}
youtubeuser();
youtubechannel();
