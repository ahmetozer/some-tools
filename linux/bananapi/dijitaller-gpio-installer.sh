#!/bin/bash
cd
mkdir dijitaller-bananpi-gpio-installer
cd dijitaller-bananpi-gpio-installer
echo "Merhaba ..."
sleep 0.5
echo "Kurulum birazdan başlıyacak. Sizden parola girmeniz istenebilir."
echo "Lütfen parolanızı doğru girin"
sleep 1
sudo apt-get install -y git
git clone https://github.com/LeMaker/RPi.GPIO_BP -b bananapi
sudo apt-get update
sudo apt-get install python-dev
cd RPi.GPIO_BP
python setup.py install                 
sudo python setup.py install


cd ..
cd dijitaller-bananpi-gpio-installer
git clone https://github.com/LeMaker/WiringBP -b bananapi
cd WiringBP/
sudo chmod +x ./build
sudo ./build
echo "Gpio pinlerinizin bilgileri"
gpio readall

cd
rm -rf dijitaller-bananpi-gpio-installer

echo "kurulum tamamlandı."
echo "Daha fazlası için https://Dijitaller.com"
echo "Yazan Ahmet OZER  https://ahmetozer.org"
