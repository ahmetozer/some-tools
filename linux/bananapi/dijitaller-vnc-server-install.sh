#!/bin/bash
set -e
NC='\033[0m' # No Color
LRed='\033[1;31m'
LGreen='\033[1;32m'
parolavnc=`date +%s | sha256sum | base64 | head -c 16 ; echo`
clear
printf "${LRed}Dijitaller.com VNC Kurlumu ${NC} \n"
echo "Merhaba"
sleep 0.5
echo "Birazdan bilgisayarınıza vnc server kurulumu başlıyacak"
echo "Bu işlemler sırasında SUDO sizden parola girmenizi isteyebilir. Lütfen parolanızı doğru girin"
echo "Kurulum başlıyor"
echo "#############"
echo "Depo güncelleniyor"
sudo apt-get update -qq -o=Dpkg::Use-Pty=0
echo "Depo güncellendi"
echo "#############"
echo "VNC sunucusu kurluyor"
sudo apt-get install -qq -o=Dpkg::Use-Pty=0 x11vnc -y > /dev/null 2>&1
sudo touch /usr/bin/d-vncserver
sudo echo "sudo killall x11vnc -9 > /dev/null 2>&1" > /usr/bin/d-vncserver
sudo echo "x11vnc -6 -shared -forever -desktop Dijitaller-com -rfbauth /etc/.vncpasswd > /dev/null 2>&1" >> /usr/bin/d-vncserver
sudo chmod 755 /usr/bin/d-vncserver
sudo rm -rf /etc/.vncpasswd > /dev/null 2>&1 || echo
sudo x11vnc -storepasswd $parolavnc /etc/.vncpasswd > /dev/null 2>&1
sudo chmod 755 /etc/.vncpasswd

sudo rm /etc/xdg/autostart/Dijitaller-vnc.desktop > /dev/null 2>&1 || echo
sudo echo "[Desktop Entry]" >> /etc/xdg/autostart/Dijitaller-vnc.desktop
sudo echo "Type=Application" >> /etc/xdg/autostart/Dijitaller-vnc.desktop
sudo echo "Name=Dijitaleler-vncserver" >> /etc/xdg/autostart/Dijitaller-vnc.desktop
sudo echo "Exec=/usr/bin/d-vncserver" >> /etc/xdg/autostart/Dijitaller-vnc.desktop
sudo echo "Comment=Share-screen" >> /etc/xdg/autostart/Dijitaller-vnc.desktop
sudo echo "X-GNOME-Autostart-enabled=true" >> /etc/xdg/Dijitaller-vnc.desktop
sudo chmod 755 /etc/xdg/Dijitaller-vnc.desktop

printf "${NC} Vnc sunucusu kuruldu. VNC sunucusuna girerken kullanacağınız parola ${LGreen} ${parolavnc} ${NC} . \n"
echo "d-vncserver komutu ile vnc sunucunuzu başlatabilirsiniz. "
echo "Masaüstü ortamınız başadığında vnc sunucunuz otamatik başlayacaktır."
echo "Parolanızı yenilemek için bu scripti tekrar çalıştırabilirsiniz"
