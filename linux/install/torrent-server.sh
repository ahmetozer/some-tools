#!/bin/bash
# Init

NC='\033[0m' # No Color
LRed='\033[1;31m'
LGreen='\033[1;32m'
clear
cd

FILE="/tmp/out.$$"
GREP="/bin/grep"
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "Bu komut sadece root izinleri ile çalışabilir. Başına sudo ekleyerek tekrar deneyin" 1>&2
   exit 1
fi
# ...
set -e
sudo apt-get update >> dijital.log 2>&1
sudo apt-get install deluge deluge-web -y >> dijital.log 2>&1
