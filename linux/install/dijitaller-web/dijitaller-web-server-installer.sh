#!/bin/bash
set -e
NC='\033[0m' # No Color
LRed='\033[1;31m'
LGreen='\033[1;32m'
parolamysql=`date +%s | sha256sum | base64 | head -c 16 ; echo`
clear


# Init
FILE="/tmp/out.$$"
GREP="/bin/grep"
#....
# Make sure only root can run our script
if [ "$(id -u)" != "0" ]; then
   echo "Bu script sadece ROOT iken çalışır" 1>&2
   exit 1
fi
# ...


cd
printf "${LRed}Dijitaller.com Web server Kurlumu ${NC} \n"
echo "Merhaba"
sleep 0.5
echo "Birazdan web sunucu kurulumu başlıyacak"
echo "Kurulumu takip etmek isterseniz tail -f web-install.log komutunu kullanın"
echo "Kurulum başlıyor"
echo "#############"
echo "Depo güncelleniyor"
sudo apt-get update -qq -o=Dpkg::Use-Pty=0
echo "Depo güncellendi"
echo "#############"

echo "Nginx web sunucusu kurluyor"
sudo apt-get install nginx debconf-utils -y >> web-install.log 2>&1
echo "Mysql kuruluyor"
echo "mysql-server mysql-server/root_password password $parolamysql" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $parolamysql" | debconf-set-selections

sudo apt-get -y install mysql-server >> web-install.log 2>&1

printf "${NC} Mysql Server kuruldu mysql parola ${LGreen} ${parolamysql} ${NC} . \n"

echo "PHP server ve eklentileri kuruluyor"
apt-get install php-curl php-gd php-mbstring php-mcrypt php-xml php-xmlrpc unzip -y >> web-install.log 2>&1
apt-get install php7.0-fpm php-gd php-gettext php-mbstring php-mcrypt php-mysql php-pear php-phpseclib php-xml php-xmlrpc php7.0-cli php7.0-common php7.0-curl php7.0-gd php7.0-json php7.0-mbstring php7.0-mcrypt php7.0-mysql php7.0-opcache php7.0-readline php7.0-xml php7.0-xmlrpc libaprutil1-ldap libjs-cropper libjs-mediaelement libjs-prototype libjs-scriptaculous liblua5.1-0 libphp-phpmailer php-getid3 -y > web-install.log 2>&1
echo "PHP server ve eklentileri kuruldu"
echo 
echo "PhpMyAdmin Kuruluyor"
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect none" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-user string root" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $parolamysql" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/app-pass password $parolamysql" |debconf-set-selections
echo "phpmyadmin phpmyadmin/app-password-confirm password $parolamysql" | debconf-set-selections
apt-get install phpmyadmin -y >> web-install.log 2>&1

printf "${NC} PhpMyAdmin kuruldu. parola ${LGreen} ${parolamysql} ${NC} . \n"

echo "openssl dhparam.pem oluşturuluyor."
openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048 >> web-install.log 2>&1

echo "Php Ayar yapılıyor"
sed -i '/opcache.enable=/c\opcache.enable=1' /etc/php/7.0/fpm/php.ini
sed -i '/opcache.memory_consumption=/c\opcache.memory_consumption=128' /etc/php/7.0/fpm/php.ini
sed -i '/opcache.max_accelerated_files=/c\opcache.max_accelerated_files=4000' /etc/php/7.0/fpm/php.ini
sed -i '/opcache.revalidate_freq=/c\opcache.revalidate_freq=60' /etc/php/7.0/fpm/php.ini
sed -i '/post_max_size/c\post_max_size = 800M' /etc/php/7.0/fpm/php.ini
sed -i '/upload_max_filesize/c\upload_max_filesize = 2000M' /etc/php/7.0/fpm/php.ini
sed -i '/mail.add_x_header =/c\mail.add_x_header = off' /etc/php/7.0/fpm/php.ini
echo "include=/etc/dijitaller/web/php-fpm/*.conf" >> /etc/php/7.0/fpm/php-fpm.conf
echo "Php Ayarı yapıldı"

mkdir -p /web >> web-install.log 2>&1
echo "dijitaller-web-wwwpermission kuruluyor"
touch /usr/bin/dijitaller-web-wwwpermission >> web-install.log 2>&1
echo '#!/bin/bash' > /usr/bin/dijitaller-web-wwwpermission
echo "chown -R www-data:www-data /web" >> /usr/bin/dijitaller-web-wwwpermission
echo "chmod -R g+rwX /web" >> /usr/bin/dijitaller-web-wwwpermission
echo "chmod 600  /etc/msmtprc" >> /usr/bin/dijitaller-web-wwwpermission
echo "chown www-data:www-data  /etc/msmtprc" >> /usr/bin/dijitaller-web-wwwpermission
echo "chown www-data:www-data -R /etc/php/7.0/fpm/pool.d" >> /usr/bin/dijitaller-web-wwwpermission
echo "chmod 600  /etc/dijitaller/web/*/mail.conf" >> /usr/bin/dijitaller-web-wwwpermission
echo "chown www-data:www-data   -R /etc/dijitaller/web" >> /usr/bin/dijitaller-web-wwwpermission
chmod 777 /usr/bin/dijitaller-web-wwwpermission

echo "dijitaller-log-temizle kuruluyor"
echo "truncate logfile --size 0 /var/log/*/*/*/*/*" > /usr/bin/dijitaller-log-temizle
echo "truncate logfile --size 0 /var/log/*/*/*/*" >> /usr/bin/dijitaller-log-temizle
echo "truncate logfile --size 0 /var/log/*/*/*" >> /usr/bin/dijitaller-log-temizle
echo "truncate logfile --size 0 /var/log/*/*" >> /usr/bin/dijitaller-log-temizle
echo "truncate logfile --size 0 /var/log/*" >> /usr/bin/dijitaller-log-temizle

echo "rm -rf /var/log/*.gz" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*.1" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*.2" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*.bz2" >> /usr/bin/dijitaller-log-temizle

echo "rm -rf /var/log/*/*.gz" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*.1" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*.2" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*.bz2" >> /usr/bin/dijitaller-log-temizle

echo "rm -rf /var/log/*/*/*.gz" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*.1" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*.2" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*.bz2" >> /usr/bin/dijitaller-log-temizle

echo "rm -rf /var/log/*/*/*/*.gz" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*/*.1" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*/*.2" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*/*.bz2" >> /usr/bin/dijitaller-log-temizle

echo "rm -rf /var/log/*/*/*/*/*.gz" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*/*/*.1" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*/*/*.2" >> /usr/bin/dijitaller-log-temizle
echo "rm -rf /var/log/*/*/*/*/*.bz2" >> /usr/bin/dijitaller-log-temizle
echo "dijitaller-log-temizle kuruldu"

echo $parolamysql > mysql.parola
echo "PhpMyAdmin SSL sertifikası oluşturuluyor"
mkdir -p /etc/dijitaller/sertifikalar/phpmyadmin >> web-install.log 2>&1
sudo openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/dijitaller/sertifikalar/phpmyadmin/nginx-selfsigned.key -out /etc/dijitaller/sertifikalar/phpmyadmin/nginx-selfsigned.crt -subj "/C=US/ST=Denial/L=Springfield/O=Dis/CN=www.example.com" >> web-install.log 2>&1

echo "PhpMyAdmin nginx ayarı oluşturuluyor"
cd
mkdir -p dijitallerclean >> web-install.log 2>&1
cd dijitallerclean
rm * >> web-install.log 2>&1 || echo 
phpmyadresi=`date +%s | sha256sum | base32 | head -c 6 ; echo`

mkdir -p /etc/nginx/snippets/

echo '
ssl_certificate /etc/dijitaller/sertifikalar/phpmyadmin/nginx-selfsigned.crt;
ssl_certificate_key /etc/dijitaller/sertifikalar/phpmyadmin/nginx-selfsigned.key;     
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
ssl_ecdh_curve secp384r1;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;
#ssl_stapling on;
ssl_stapling_verify on;
resolver 8.8.8.8 8.8.4.4 valid=300s;
resolver_timeout 5s;
# Disable preloading HSTS for now.  You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
#add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
add_header Strict-Transport-Security "max-age=63072000; includeSubdomains";
add_header X-Frame-Options DENY;
add_header X-Content-Type-Options nosniff;

ssl_dhparam /etc/ssl/certs/dhparam.pem;
'>>/etc/nginx/snippets/own-ssl.conf

echo 'server {
    listen 80;
    listen [::]:80;
    server_name phpmyadminsifresiz.*;
    return 301 https://$host;
}

server {
#add_header X-Frame-Options "SAMEORIGIN";
server_name phpmyadminsifresiz.*;
    listen 443 ssl http2;
    listen [::]:443 ssl http2;
    include snippets/own-ssl.conf;
    client_max_body_size 100M;
    root /usr/share/phpmyadmin;
    index index.html index.htm index.php;
    charset utf-8;
  index index.php;
  location / {
    try_files $uri $uri/ /index.php?$args;
  }
  location ~* \.(jpg|jpeg|gif|css|png|js|ico|html)$ {
    access_log off;
    expires max;
  }
  location ~ /\.ht {
    deny  all;
  }
  location ~ \.php$ {
    fastcgi_index index.php;
    fastcgi_keep_conn on;
    include /etc/nginx/fastcgi.conf;
    fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
	   fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
  }
}
'>> nginx.wa

sed 's/phpmyadminsifresiz/'$phpmyadresi'/g' nginx.wa >> /etc/nginx/sites-enabled/phpmyadmin.web
cd 
rm -rf dijitallerclean
printf "${NC} Nginx PhpMyAdmin ayarı oluşturuldu. PhpMyAdmin erişmek için  ${LGreen} ${phpmyadresi}${NC}.example.com üzerinden erişebilirsiniz. \n"
printf "${NC} Cloudflare yada dns sunucunuza  ${LGreen} ${phpmyadresi}${NC} domain adresini eklemeyi unutmayın. \n"

mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.dijitaller.old
echo 'user www-data;
worker_processes auto;
pid /run/nginx.pid;

events {
        worker_connections 768;
        # multi_accept on;
}

http {

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;
        gzip_disable "msie6";

        # gzip_vary on;
        # gzip_proxied any;
        # gzip_comp_level 6;
        # gzip_buffers 16 8k;
        # gzip_http_version 1.1;
        # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;
								
####           ###
#  CACHE ayarı   #
####           ###
fastcgi_cache_path /web/CACHE levels=1:2 keys_zone=CACHE:30m inactive=30s;
fastcgi_cache_key "$scheme$request_method$host$request_uri";
fastcgi_cache_use_stale error timeout invalid_header http_500;
#fastcgi_ignore_headers Cache-Control Expires Set-Cookie;

####           ###
#  DDOS  ayarı   #
####           ###
limit_req_zone $binary_remote_addr zone=antiddosphp:10m rate=1r/s;
limit_req_zone $binary_remote_addr zone=antiddos:10m rate=10r/s;
limit_req_zone $binary_remote_addr zone=one:10m rate=1r/s;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;
	include	/etc/dijitaller/web/*/nginx.conf;
}


#mail {
#       # See sample authentication script at:
#       # http://wiki.nginx.org/ImapAuthenticateWithApachePhpScript
#
#       # auth_http localhost/auth.php;
#       # pop3_capabilities "TOP" "USER";
#       # imap_capabilities "IMAP4rev1" "UIDPLUS";
#
#       server {
#               listen     localhost:110;
#               protocol   pop3;
#               proxy      on;
#       }
#
#       server {
#               listen     localhost:143;
#               protocol   imap;
#               proxy      on;
#       }
#}
' >> /etc/nginx/nginx.conf



apt-get install msmtp -y >> web-install.log 2>&1
touch /etc/msmtprc

mkdir -p /Backup
echo "mysqldump -u root -p$parolamysql --all-databases | gzip > /Backup/database_`date +gun%d-ay-%m-yil%y-saat%H-dakika%M-saniye%S`.sql.gz" >> /usr/bin/dijitaller-sql-yedek
chmod 755 /usr/bin/dijitaller-sql-yedek


wget https://raw.githubusercontent.com/AhmetOZER/some-tools/master/linux/install/dijitaller-web/dijitaller-site-yap.sh --no-check-certificate >> web-install.log 2>&1
mv dijitaller-site-yap.sh /usr/bin/dijitaller-site-olustur
chmod 755 /usr/bin/dijitaller-site-olustur

mkdir -p /etc/dijitaller/web/
mkdir -p /etc/dijitaller/web/php-fpm/
service nginx restart
service mysql restart
service php7.0-fpm restart

echo "Buraya site oluşturucu scriptleri yazacaksın"
printf "${NC} Mysql Server ve PhpMyAdmin  girerken kullanacağınız root parolası ${LGreen} ${parolamysql} ${NC} \n"
printf "${NC} PhpMyAdmin erişmek için  ${LGreen} ${phpmyadresi}${NC}.example.com üzerinden erişebilirsiniz. \n"
printf "${NC} Cloudflare yada dns sunucunuza  ${LGreen} ${phpmyadresi}${NC} domain adresini eklemeyi unutmayın. \n"
echo "Kurulum tamamlandı"
echo "Site oluşturmak için dijitaller-site-olustur komutunu çalıştırın"
echo "Bu Script Dijitaller.com için Ahmet ÖZER tarafından yazılmıştır."
echo "Tüm hakları saklıdır. https://www.dijitaller.com https://ahmetozer.org"
