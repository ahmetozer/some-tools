iptables --table nat --append POSTROUTING --out-interface eth0 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface eth0 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tun0 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tun0 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tap0 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tap0 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface ppp0 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface ppp0 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tun1 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tun1 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tap1 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tap1 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tap2 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tap2 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tap3 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tap3 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tun2 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tun2 -j ACCEPT > /dev/null

iptables --table nat --append POSTROUTING --out-interface tun3 -j MASQUERADE > /dev/null
iptables --append FORWARD --in-interface tun3 -j ACCEPT > /dev/null

sysctl -p

